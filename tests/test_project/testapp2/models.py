from django.db import models

from elasticsearch_lite.mixins import ESIndexedModelMixin


class ExampleModelInAnotherAPP(models.Model, ESIndexedModelMixin):
    noname = models.CharField('Name', max_length=100)
